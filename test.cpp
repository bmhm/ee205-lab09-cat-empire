///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file test.cpp
/// @version 1.0
///
/// unit test
///
/// @author Brooke Maeda <bmhm@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   22 April 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "cat.hpp"

using namespace std;

int main(){

   CatEmpire empire;
   //testing empty
   cout << "is empty? " << boolalpha << empire.empty() << endl;
  
   //testing addcat(s)
   cout << "adding one cat" << endl;
   empire.addCat( new Cat("catty") );
   cout << "is empty? " << boolalpha << empire.empty() << endl;

   //testing getEnglishSuffix
   for( int i = 1; i < 41; i++ ){
      empire.getEnglishSuffix(i);
      cout << " Generation" << endl;
   }


}

